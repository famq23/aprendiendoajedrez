@component('mail::message')
# {{ __("Nuevo estudiante inscrito en tu curso") }}

{{ __("Nombre estudiante: :student", ['student' => $student]) }} <br>
{{ __("Curso inscrito: :course", ['course' => $course->name]) }} <br>


{{-- __("El estudiante :student se ha inscrito en tu curso :course, FELICIDADES", ['student' => $student, 'course' => $course->name]) --}}
<img class="img-responsive" src="{{ url('storage/courses/' . $course->picture) }}" alt="{{ $course->name }}">

@component('mail::button', ['url' => url('/courses/' . $course->slug), 'color' => 'blue'])
{{ __("Ir al curso") }}
@endcomponent

{{ __("Felicidades y gracias") }},<br>
{{ config('app.name') }}

@endcomponent
